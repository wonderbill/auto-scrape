# README #

At WonderBill we do a lot of data collection and processing from third-party
websites. The quality and reliability of these sites can vary greatly. We use
scripts, similar to those you might create for end-to-end testing, in order to
navigate these sites and extract relevant information.

### Task Definition ###

We would like you to retrieve some useful information from our own legacy web
app. This should be performed with a script powered by a browser
automation tool, such as Puppeteer, Selenium, etc., use one you are familiar
with! Account credentials will be provided. URL of the web app is:
[my.wonderbill.com](https://my.wonderbill.com).

### Requirements ###

* Is runnable through the command line
* Can log in to the WonderBill website using credentials provided via CLI
arguments
* Written in JavaScript
* Gathers the following information on each account belonging to the
user:
    * Name
    * Amount
    * Payment date
* For each account, the amount spent so far this year, and the amount left to
pay this year should be calculated (assume all accounts are paid monthly)
* After gathering all account data, it should be output as valid JSON and
follow the pattern shown in the example below:
```
{
    "accounts": [
        {
            "name": "My First Account",
            "amount": "£12.34",
            "lastPaymentDate": "1970-01-01",
            "paidYearAmount": "£123.45",
            "outstandingYearAmount": "£123.45"
        },
        {
            "name": "My Second Account",
            "amount": "£12.34",
            "lastPaymentDate": "1970-01-01",
            "paidYearAmount": "£123.45",
            "outstandingYearAmount": "£123.45"
        }
    ]
}
```

### Submission ###

Please provide a URL to a public repository containing your task submission.
Instructions on how to run the script are useful to include.

### Bonus Points ###

Taking into account that websites do change or can be temporarily unavailable,
add something that would aid in the identification of what went wrong, e.g.
logs, DOM snapshots, screenshots, video, etc.

### Thank you for your time and effort! ###

*Please note, the account provided to you is only for use during this task and
usage of our web app is subject to the standard terms and conditions found
[here](https://www.wonderbill.com/terms.html).*
